# PowerNSX #

## About ##
PowerNSX is a PowerShell module that abstracts the VMware NSX API to a set of easily used PowerShell functions.

This module is _not supported_ by VMware, and comes with no warranties express or implied.  Please test and validate its functionality before using in a production environment.

It aims to focus on exposing New, Update, Remove and Get operations for all key NSX functions as well as adding additional functionality to extend the capabilities of NSX management beyond the native UI or API.  

It is unlikely that it will ever expose 100% of the NSX API, but feature requests are welcomed if you find a particular function you require to be lacking.

PowerNSX is currently a work in progress and is not yet feature complete. 

See the Wiki for Setup and Usage instructions

## Contribution guidelines ##

Feature requests are more than welcome, just dont get your hopes up. :)
Patches are welcome for bugs/new functionality.

## Who do I talk to? ##

Im just one guy but feel free to contact me at nbradford@vmware.com.